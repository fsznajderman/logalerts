/**
 * Created by fsznajderman on 10/07/2014.
 */

//mapping handlebar template


var mapping = {};
mapping["create-http-alert"] = {"label": "Création alert HTTP", "templateFile": "httpAlert", "id": "createHttpAlert", "cache": "", "listable": true, localstore: true, finalInitFunction: function () {
    submitForm('createAlertHttp');
}};
mapping["display-alert-items"] = { "templateFile": "displayAlertItem", "id": "alert-items-template", "cache": "", listable: false, localstore: false};

$(function () {


    //fix issue regarding browser caching
    var cache = new Date();
    //add area to store template when is needed
    $("body").append("<div id='templateArea' style='display:none;' ></div>");

    _.each(_.keys(mapping), function (key) {
        var currentConf = mapping[key];
        if (!_.isUndefined(currentConf) && _(currentConf.cache).blank()) {
            $.get("page/templates/" + currentConf.templateFile + ".html?" + cache.getMilliseconds(),
                function (data) {
                    if (currentConf.localstore) {
                        $('#templateArea').append(data);
                    }
                    currentConf.cache = hc(data);
                    run(currentConf);
                });
        }

    })

})


function getTemplate(key, context) {
    return mapping[key].cache(context);
}

function run(currentConf) {
    if (_.contains(_.keys(currentConf), "finalInitFunction")) {
        currentConf.finalInitFunction();
    }

}


function hc(content) {
    return Handlebars.compile(content);
}

/**
 * Submit for without reload page
 * @param idForm
 */
function submitForm(form) {
    var idForm = "#" + form;
    $(idForm).submit(function (event) {
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            dataType: 'json',
            data: JSON.stringify($(this).serialize()),
            contentType: "application/json",
            traditional: true,
            success: function (data) {
                $.notify("La mise à jour été effectuée avec succès.", "success");
            },
            error: function (xhr, err) {
                $.notify("Une erreur est survenue lors de l'enregistrement...", "error");
            }
        });
        event.preventDefault();
    });
}
