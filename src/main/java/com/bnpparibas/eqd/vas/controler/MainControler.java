package com.bnpparibas.eqd.vas.controler;

import com.bnpparibas.eqd.vas.dao.AlertHttpRepository;
import com.bnpparibas.eqd.vas.dao.config.SpringAppContextConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * User: 855522 Date: 26/06/2014
 */
@WebServlet("/testConfLoag4")
public class MainControler extends HttpServlet implements WebApplicationInitializer {

    final static Logger LOG = LogManager.getLogger(MainControler.class.getName());


    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        LOG.debug("---->Start Load Conf");

        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
        rootContext.scan("com.bnpparibas.eqd.vas");
        rootContext.register(SpringAppContextConfiguration.class);

        rootContext.refresh();

        for(String s : rootContext.getBeanDefinitionNames()) {
            LOG.info("Bean = " +s);
        }




        /*
        // Create the 'root' Spring application context
      AnnotationConfigWebApplicationContext rootContext =
        new AnnotationConfigWebApplicationContext();
      rootContext.register(AppConfig.class);

      // Manage the lifecycle of the root application context
      container.addListener(new ContextLoaderListener(rootContext));

      // Create the dispatcher servlet's Spring application context

      dispatcherContext.register(DispatcherConfig.class);

      // Register and map the dispatcher servlet
      ServletRegistration.Dynamic dispatcher =
        container.addServlet("dispatcher", new DispatcherServlet(dispatcherContext));
      dispatcher.setLoadOnStartup(1);
      dispatcher.addMapping("/");
         */


        LOG.debug("end Load Conf<------");
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
            IOException {
        LOG.debug("Application started!");
    }
}
