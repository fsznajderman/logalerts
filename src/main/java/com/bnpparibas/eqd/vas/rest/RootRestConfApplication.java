package com.bnpparibas.eqd.vas.rest;

import com.google.common.collect.Sets;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Set;

/**
 * User: 855522
 * Date: 30/06/2014
 */
@ApplicationPath("/rest-api")
public class RootRestConfApplication extends Application{

    final static Logger LOG = LogManager.getLogger(RootRestConfApplication.class.getName());

    @Override
    public Set<Class<?>> getClasses() {
        LOG.trace("load Classes");
        return Sets.newHashSet(AlertsRestApi.class);
    }
}
