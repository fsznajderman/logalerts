package com.bnpparibas.eqd.vas.rest;

import com.bnpparibas.eqd.vas.dao.AlertHttpRepository;
import com.bnpparibas.eqd.vas.model.Alert;
import com.bnpparibas.eqd.vas.model.HttpAlert;
import com.google.common.collect.Lists;
import com.owlike.genson.Genson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;

/**
 * User: 855522 Date: 30/06/2014
 */
@Path("/alerts")
@Component
public class AlertsRestApi {


    @Autowired
    private AlertHttpRepository alertHttpRepository;

    final static Logger LOG = LogManager.getLogger(AlertsRestApi.class.getName());

    @GET
    @Produces("text/html")
    public String getAllAlerts() {
          LOG.debug("alertHttpRepository : " + alertHttpRepository);

        final Alert a = new HttpAlert(1l,"", "", "", "", "", "", "Alert 1");
        final Alert b = new HttpAlert(1l,"", "", "", "", "", "", "Alert 2");

        return new Genson().serialize(Lists.newArrayList(a, b));
    }


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void saveHttpAlert(String message) {

        LOG.debug("post : " + message);
        HttpAlert httpAlert = new Genson().deserialize(message, HttpAlert.class);
        //Arrays.asList(message.split("&")).stream().forEach(s -> System.out.println(s));

    }

}
