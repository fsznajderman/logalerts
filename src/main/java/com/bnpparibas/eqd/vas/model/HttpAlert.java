package com.bnpparibas.eqd.vas.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * User: 855522 Date: 02/07/2014
 */

@Entity
public class HttpAlert implements Alert,Serializable {

    @Id
    private Long id;

    private String baseUrl;

    private String path;

    private String method;

    private String codePattern;

    private String description;

    private String cronExpression;

    private String label;


    public HttpAlert() {
    }

    public HttpAlert(Long id, String baseUrl, String path, String method, String codePattern, String description, String cronExpression, String label) {
        this.id = id;
        this.baseUrl = baseUrl;
        this.path = path;
        this.method = method;
        this.codePattern = codePattern;
        this.description = description;
        this.cronExpression = cronExpression;
        this.label = label;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getCodePattern() {
        return codePattern;
    }

    public void setCodePattern(String responseCode) {
        this.codePattern = responseCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
