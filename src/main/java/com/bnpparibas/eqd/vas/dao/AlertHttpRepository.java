package com.bnpparibas.eqd.vas.dao;

import com.bnpparibas.eqd.vas.model.HttpAlert;
import org.springframework.data.repository.Repository;
import org.springframework.stereotype.Component;

/**
 * Created by fsznajderman on 15/07/2014.
 */


public interface AlertHttpRepository extends Repository<HttpAlert,Long> {}
